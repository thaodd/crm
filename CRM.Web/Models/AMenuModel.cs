﻿using CRM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Web.Models
{
    public class AMenuModel
    {
        public AMenuModel()
        {

        }
        public AMenuModel(AMenu entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Icon = entity.Icon;
            Link = entity.Link;
        }
        public static List<AMenuModel> ConvertToModel(List<AMenu> entities)
        {
            var models = new List<AMenuModel>();
            foreach(var entity in entities)
            {
                var model = new AMenuModel(entity);
                models.Add(model);
            }
            return models;
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Link { get; set; }
    }
}