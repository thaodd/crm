﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Web.Models
{
    public class PagingModel
    {
        public PagingModel(int pagenumber, int pagesize, int totalrows, int rowstart, int rowend, string href, int tabIndex = 1)
        {
            this.PageNumber = pagenumber;
            this.PageSize = pagesize;
            this.TotalRow = totalrows;
            this.RowStart = rowstart;
            this.RowEnd = rowend;
            this.Href = href;
            this.TabIndex = tabIndex;
            this.TotalPage = this.CalcTotalPage();
        }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalRow { get; set; }
        public int TotalPage { get; set; }
        public int RowStart { get; set; }
        public int RowEnd { get; set; }
        public string Href { get; set; }
        public int TabIndex { get; set; }


        private int CalcTotalPage()
        {
            int totalpage = this.TotalRow % this.PageSize == 0 ? (this.TotalRow / this.PageSize) : (this.TotalRow / this.PageSize) + 1;
            return totalpage;
        }
    }
}