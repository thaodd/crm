﻿using CRM.Bussiness;
using CRM.Common;
using CRM.Entities;
using CRM.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Web.Controllers
{
    public class AMenuController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.PageNumber = Setting.PageNumber;
            ViewBag.PageSize = Setting.PageSize;
            //
            return View();
        }
        [HttpPost]
        public ActionResult List(FindModel model)
        {
            AMenuComponents _ObjComponents = new AMenuComponents();
            int count = 0;
            var ret = _ObjComponents.Select(model.Keyword, ref count, model.PageNumber, model.PageSize);
            if (count > 0)
            {
                int rowStart = ((model.PageNumber - 1) * model.PageSize) + 1;
                int rowEnd = model.PageNumber * model.PageSize;
                //
                PagingModel paging = new PagingModel(model.PageNumber, model.PageSize, count, rowStart, rowEnd, "#");
                ViewBag.PagingModel = paging;
            }
            return this.PartialView(AMenuModel.ConvertToModel(ret));
        }
        [HttpPost]
        public int Update(AMenuModel model)
        {
            try
            {
                int iVal = 0;
                AMenuComponents _ObjComponents = new AMenuComponents();
                var _Obj = _ObjComponents.GetById(model.Id);
                if (_Obj == null)
                {
                    _Obj = new AMenu();
                    //_Obj.CreatedUserId = int.Parse(HttpContext.User.Identity.Name);
                    //_Obj.CreatedDate = DateTime.Now;
                }
                _Obj.Title = model.Title;
                //_Obj.ContentFull = model.ContentFull;
                //_Obj.MetaDescription = model.MetaDescription;
                //_Obj.MetaKeywords = model.MetaKeywords;
                //_Obj.MetaTitle = model.MetaTitle;
                //_Obj.LastUpdatedUserId = int.Parse(HttpContext.User.Identity.Name);
                //_Obj.LastUpdatedDate = DateTime.Now;
                //
                if (_Obj.Id > 0)
                    iVal = _ObjComponents.Update(_Obj);
                else
                {
                    iVal = _ObjComponents.Add(_Obj);
                }
                //
                return (int)EnumAlert.Thanhcong;
            }
            catch (Exception ex)
            {
                AdminHelper.LogEvent(DateTime.Now + ": " + ex.Message);
                return (int)EnumAlert.Khongxacdinh;
            }
        }

        public ActionResult Edit(int id)
        {
            AMenuComponents _ObjComponents = new AMenuComponents();
            var _Obj = _ObjComponents.GetById(id);
            if (_Obj == null) _Obj = new Entities.AMenu();
            AMenuModel model = new AMenuModel(_Obj);

            //
            return View(model);
        }
        [HttpPost]
        public int Delete(int id)
        {
            int iVal = 0;
            AMenuComponents _ObjComponents = new AMenuComponents();
            var _Obj = _ObjComponents.GetById(id);
            if (_Obj != null)
            {
                if (_ObjComponents.Delete(_Obj))
                    iVal = 1;
            }
            return iVal;
        }
    }
}