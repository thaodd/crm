﻿using CRM.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CRM.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override void ExecuteCore()
        {

            string culture = "vi-VN";
            if (this.Session == null || this.Session[Setting.CurrentCulture] == null)
            {
                this.Session[Setting.CurrentCulture] = culture;
            }
            else
            {
                culture = this.Session[Setting.CurrentCulture].ToString();
            }
            //
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            //
            base.ExecuteCore();
        }
        protected override bool DisableAsyncSupport
        {
            get { return true; }
        }
    }
}
