﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Web.Controllers
{
    public class NavController : Controller
    {
        // GET: Home
        public ActionResult AHeader()
        {
            return View();
        }
        public ActionResult AFooter()
        {
            return View();
        }
        public ActionResult ASidebar()
        {
            return View();
        }
    }
}