﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Common
{
    public static class Setting
    {
        public static string CurrentCulture = "culture";
        public static int PageNumber = 1;
        public static int PageSize = 10;
    }
}
